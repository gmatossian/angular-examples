angular.module('ContactsApp')
.service('ContactsSvc', function($http) {
    var self = this;
    
    self.getContacts = function() {
        return $http.get('http://localhost:3000/contacts');
    };
    
    self.addContact = function(contact) {   // Returns the new object
        return $http.post('http://localhost:3000/contacts', contact);
    };
    
    self.updateContact = function(contact) {   // Returns the updated object
        return $http.put('http://localhost:3000/contacts/' + contact.id, contact);
    };
    
    self.removeContact = function(contact) {
        return $http.delete('http://localhost:3000/contacts/' + contact.id);
    };
});