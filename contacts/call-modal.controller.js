angular.module('ContactsApp')
.controller('CallModalCtrl', function($uibModalInstance, params) {
    var self = this;

    self.phoneNumber = params.phoneNumber;

    self.close = function() {
        $uibModalInstance.close();
    };
});