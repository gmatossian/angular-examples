angular.module('ContactsApp')
.config(function($routeProvider) {
    $routeProvider
    .when("/", {
        templateUrl: "main.htm"
    })
    .when("/form", {
        templateUrl: "form.htm"
    })
    .when("/compose", {
        templateUrl: "compose.htm"
    });
});