# Angular examples

This repo contains some angular applications.

## Getting Started

### Organization

Each directory (at the root level) contains an Angular application.
e.g. the **contacts** directory contains the *Contacts* application.

### Running

To run a particular example, start **http-server** on the directory:

```
$ cd contacts
$ http-server
```

Additionally, if the example uses a REST API, you'll have to start **json-server** as well:

```
$ cd contacts
$ json-server data/db.json
```

## Built With

* [Angular 1.6.4](http://www.dropwizard.io/1.0.2/docs/)
* [Bootstrap 3.3.7](http://getbootstrap.com/)
* [UI Bootstrap 2.5.0](https://angular-ui.github.io/bootstrap/)

## Authors

* **Gabriel Matossian** - [gmatossian](https://bitbucket.org/gmatossian/)