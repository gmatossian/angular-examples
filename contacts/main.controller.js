angular.module('ContactsApp')
.controller('MainCtrl', function(ContactsSvc, $location, SharedData, $uibModal) {
    var self = this;

    self.contacts = [];
    self.filteredContacts = self.contacts;
    self.filter = '';
    self.successMessage = '';   // TODO: What's gonna happen with this? Maybe should go in a service

    // UI behaviour...

    self.call = function(phoneNumber) {
        console.log('Calling ' + phoneNumber + '...');
        
        var modalInstance = $uibModal.open({
            templateUrl: 'call-modal.htm?bust=' + Math.random().toString(36).slice(2),
            controller: 'CallModalCtrl',
            controllerAs: 'ctrl',
            size: 'sm',
            resolve: {
                params: function() {
                    return {
                        "phoneNumber": phoneNumber
                    };
                }
            }
        });
        modalInstance.result.then(
            function() {    // Modal closed
                console.log('Call ended.');
            }, function() {    // Modal dismissed
            }
        );
    };

    self.createContact = function() {
        SharedData.contact = {};
        $location.path('/form')
    };

    self.filterContacts = function() {
        self.filteredContacts = self.contacts.filter(function(contact) {
            return contact.name.toLowerCase().includes(self.filter.toLowerCase())
                || contact.organization.toLowerCase().includes(self.filter.toLowerCase());
        });
    };

    self.editContact = function(contact) {
        SharedData.contact = contact;
        $location.path('/form')
    };

    self.text = function(contact) {
        SharedData.contact = contact;
        $location.path('/compose')
    };

    self.displaySuccessMessage = function(msg) {
        self.hideErrorMessage();
        self.successMessage = msg;
    };

    self.hideSuccessMessage = function() {
        self.successMessage = '';
    };

    self.displayErrorMessage = function(msg) {
        self.hideSuccessMessage();
        self.errorMessage = msg;
    };

    self.hideErrorMessage = function() {
        self.errorMessage = '';
    };

    // CRUD functionality...

    self.removeContact = function(contact) {
        ContactsSvc.removeContact(contact).then(function() {
            self.updateContacts();
            self.displaySuccessMessage('Contact removed');
        });
    };

    self.updateContacts = function() {
        ContactsSvc.getContacts().then(function(response) {
            self.contacts = response.data;
            self.filterContacts();
        });
    };

    // Initialization...

    window.setTimeout(function() {  // TODO: Find out why it's necessary. Maybe json-server has a delay?
        self.updateContacts();
    }, 100);
});