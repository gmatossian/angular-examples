angular.module('ContactsApp')
.controller('FormCtrl', function(ContactsSvc, $location, SharedData) {
    var self = this;
    
    self.contact = SharedData.contact;
    self.successMessage = '';   // Do we need this in here?

    // UI behaviour...

    self.cancel = function() {
        SharedData.contact = {};
        $location.path('/');
    };

    self.displaySuccessMessage = function(msg) {
        self.hideErrorMessage();
        self.successMessage = msg;
    };

    self.hideSuccessMessage = function() {
        self.successMessage = '';
    };

    self.displayErrorMessage = function(msg) {
        self.hideSuccessMessage();
        self.errorMessage = msg;
    };

    self.hideErrorMessage = function() {
        self.errorMessage = '';
    };

    self.textContact = function(contact) {
        console.log('Showing compose view to text contact ' + contact);
    };

    // CRUD functionality...

    self.saveContact = function() {
        if(self.contact.id) {
            ContactsSvc.updateContact(self.contact).then(function() {
                self.displaySuccessMessage('Contact updated');
            },
            function() {
                self.displayErrorMessage('Unable to update contact');
            });
        } else {
            ContactsSvc.addContact(self.contact).then(function() {
                self.displaySuccessMessage('Contact created');
            },
            function() {
                self.displayErrorMessage('Unable to create contact');
            });
        }
        $location.path('/');
    };
});