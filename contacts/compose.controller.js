angular.module('ContactsApp')
.controller('ComposeCtrl', function(ContactsSvc, $location, SharedData) {
    var self = this;

    self.contact = SharedData.contact;
    self.message = '';

    // UI behaviour...

    self.cancel = function() {
        SharedData.contact = {};
        $location.path('/');
    };

    // 

    self.send = function() {
        console.log('Sending text message to ' + SharedData.contact.name + ': ' + self.message);
        SharedData.contact = {};
        $location.path('/');
    };
});